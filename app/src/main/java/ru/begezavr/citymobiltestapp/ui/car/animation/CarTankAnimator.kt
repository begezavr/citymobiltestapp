package ru.begezavr.citymobiltestapp.ui.car.animation

import android.animation.*
import android.graphics.Path
import android.view.animation.AccelerateDecelerateInterpolator
import ru.begezavr.citymobiltestapp.ui.car.view.CarScreenPosition


class CarTankAnimator(private val listener: (CarScreenPosition) -> Unit): CarAnimator(listener) {
    companion object {
        private const val ROTATION_DURATION = 400L
        private const val MOVE_DURATION = 400L
    }
    private var animator: ValueAnimator? = null
    private var currentPosition: CarScreenPosition = CarScreenPosition.EMPTY
    private var targetPosition: CarScreenPosition = CarScreenPosition.EMPTY

    override fun go(from: CarScreenPosition, to: CarScreenPosition) {
        animator?.cancel()
        currentPosition = from
        targetPosition = to
        startRotateAnimation(from, to)
    }

    override fun cancel() {
        animator?.cancel()
    }

    private fun startRotateAnimation(from: CarScreenPosition, to: CarScreenPosition) {
        val targetAngle = angleFromPositions(from, to)
        val pvhA = PropertyValuesHolder.ofFloat("angle", from.angle, targetAngle)
        animator = ObjectAnimator.ofPropertyValuesHolder(currentPosition, pvhA).apply {
            duration = ROTATION_DURATION
            interpolator = AccelerateDecelerateInterpolator()
            addUpdateListener {
                listener.invoke(currentPosition)
            }
            addListener(rotateAnimationListener)
        }
        animator?.start()
    }

    private val rotateAnimationListener = object: AnimatorListenerAdapter() {
        private var isCanceled = false
        override fun onAnimationCancel(animation: Animator) {
            isCanceled = true
        }
        override fun onAnimationEnd(animation: Animator) {
            currentPosition.angle = cleanAngle(currentPosition.angle)
            listener.invoke(currentPosition)
            if (!isCanceled) {
                startMoveAnimation(currentPosition, targetPosition)
            }
            isCanceled = false
        }
    }

    private fun startMoveAnimation(from: CarScreenPosition, to: CarScreenPosition) {
        val pvhX = PropertyValuesHolder.ofFloat("x", from.x, to.x)
        val pvhY = PropertyValuesHolder.ofFloat("y", from.y, to.y)
        animator = ObjectAnimator.ofPropertyValuesHolder(currentPosition, pvhX, pvhY).apply {
            duration = MOVE_DURATION
            interpolator = AccelerateDecelerateInterpolator()
            addUpdateListener {
                listener.invoke(currentPosition)
            }
        }
        animator?.start()
    }
}