package ru.begezavr.citymobiltestapp.ui.car

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.car_fragment.*
import ru.begezavr.citymobiltestapp.R

class CarFragment : Fragment() {

    companion object {
        fun newInstance() = CarFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.car_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        car_fragment_tank_button.setOnClickListener {
            car_fragment_car_view.changeToTankAnimator()
        }
        car_fragment_no_tank_button.setOnClickListener {
            car_fragment_car_view.changeToNoTankAnimator()
        }
        if (savedInstanceState == null) {
            car_fragment_car_view.post {
                car_fragment_car_view.centerCar()
            }
        }
    }
}
