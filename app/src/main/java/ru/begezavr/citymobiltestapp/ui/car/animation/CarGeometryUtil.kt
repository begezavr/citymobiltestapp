package ru.begezavr.citymobiltestapp.ui.car.animation

import ru.begezavr.citymobiltestapp.ui.car.view.CarScreenPosition


fun angleFromPositions(from: CarScreenPosition, to: CarScreenPosition): Float {
    val dx = from.x - to.x
    val dy = from.y - to.y
    val targetAngle = if (dx == 0f) {
        if (dy > 0) 0f else 180f
    } else {
        angleCorrection(from, to) + Math.toDegrees(Math.atan(dy/dx.toDouble())).toFloat()
    }
    return closestAngle(from.angle, targetAngle)
}

fun angleCorrection(from: CarScreenPosition, to: CarScreenPosition): Float {
    val dx = from.x - to.x
    return if (dx < 0) {
        90f
    } else {
        -90f
    }
}

fun closestAngle(from: Float, to: Float): Float {
    val diff = from - to
    if (Math.abs(diff) > 180) {
        return if (from > 0) {
            from + 360 - diff
        } else {
            -360 + to
        }
    }
    return to
}

fun cleanAngle(angle: Float): Float {
    val result = angle % 360
    return if (result > 180) {
        result - 360
    } else {
        result
    }
}

