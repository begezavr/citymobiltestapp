package ru.begezavr.citymobiltestapp.ui.car.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.GestureDetector
import android.view.MotionEvent
import ru.begezavr.citymobiltestapp.ui.car.animation.CarAnimator
import ru.begezavr.citymobiltestapp.ui.car.animation.CarCurveAnimator
import android.os.Parcelable
import android.os.Bundle
import ru.begezavr.citymobiltestapp.ui.car.animation.CarTankAnimator
import ru.begezavr.citymobiltestapp.util.dp


class TouchToGoCarView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    companion object {
        private const val SUPER_STATE_KEY =  "SUPER_STATE_KEY"
        private const val CAR_STATE_KEY =  "CAR_STATE_KEY"
        private const val CAR_WIDTH_DP = 30
        private const val CAR_HEIGHT_DP = 80
        private val CAR_FILL_COLOR = Color.rgb(254, 205, 103)
        private val CAR_STROKE_COLOR = Color.rgb(80, 95, 132)
        private val CAR_WIDTH = (CAR_WIDTH_DP * Resources.getSystem().displayMetrics.density)
        private val CAR_HEIGHT = (CAR_HEIGHT_DP * Resources.getSystem().displayMetrics.density)
    }

    private val minDistanceToBorder = (Math.sqrt((CAR_WIDTH * CAR_WIDTH + CAR_HEIGHT * CAR_HEIGHT).toDouble()) / 2).toInt()
    private val screenBox = RectF(-1000f, -1000f, Float.MAX_VALUE, Float.MAX_VALUE)
    private val car:Car = SimpleCar(CAR_FILL_COLOR, CAR_STROKE_COLOR)
    private var carScreenPosition = CarScreenPosition(0f, 0f, CAR_WIDTH, CAR_HEIGHT, 0f)
    private var carAnimator: CarAnimator = CarTankAnimator(::updateCarScreenPosition)
    private var pathToDraw: Path? = null
    private val paint: Paint = Paint()

    private val tapListener = object: GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            startCarAnimation(carScreenPosition, carScreenPosition.copy(x = e.x, y = e.y))
            return true
        }

        override fun onDown(e: MotionEvent?): Boolean {
            return true
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return true
        }
    }
    private val gestureDetector = GestureDetector(context, tapListener)

    fun centerCar() {
        carScreenPosition = carScreenPosition.copy(x = measuredWidth/2f, y = measuredHeight/2f, angle = 0f)
        screenBox.set(0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat())
    }

    fun changeToTankAnimator() {
        carAnimator.cancel()
        carAnimator = CarTankAnimator(::updateCarScreenPosition)
        pathToDraw = null
    }

    fun changeToNoTankAnimator() {
        carAnimator.cancel()
        carAnimator = CarCurveAnimator(::updateCarScreenPosition).apply {
            getPath {
                pathToDraw = it
            }
        }
    }

    private fun updateCarScreenPosition(newPosition: CarScreenPosition) {
        carScreenPosition = newPosition
        invalidate()
    }

    private fun startCarAnimation(from: CarScreenPosition, to: CarScreenPosition) {
        val targetPosition = if(isOnScreen(to)) {
            to
        } else {
            moveToScreen(to)
        }
        carAnimator.go(from, targetPosition)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas != null) {
            car.draw(canvas, carScreenPosition)
            pathToDraw?.let {
                paint.color = Color.BLUE
                paint.style = Paint.Style.STROKE
                paint.strokeWidth = 2.dp
                canvas.drawPath(it, paint)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(event) || super.onTouchEvent(event)
    }

    /**
     * for simplicity just check is distance from center to one of borders is less then
     * "car diagonal" half length (minDistanceToBorder)
     */
    private fun isOnScreen(pos: CarScreenPosition): Boolean {
        return pos.x - screenBox.left > minDistanceToBorder &&
                pos.y - screenBox.top > minDistanceToBorder &&
                screenBox.right - pos.x > minDistanceToBorder &&
                screenBox.bottom - pos.y > minDistanceToBorder
    }

    private fun moveToScreen(to: CarScreenPosition): CarScreenPosition {
        if (to.x - screenBox.left < minDistanceToBorder) {
            to.x = screenBox.left + minDistanceToBorder
        }
        if (screenBox.right - to.x < minDistanceToBorder) {
            to.x = screenBox.right - minDistanceToBorder
        }
        if (to.y - screenBox.top < minDistanceToBorder) {
            to.y = screenBox.top + minDistanceToBorder
        }
        if (screenBox.bottom - to.y < minDistanceToBorder) {
            to.y = screenBox.bottom - minDistanceToBorder
        }
        return to
    }

    public override fun onSaveInstanceState(): Parcelable? {
        return Bundle().apply {
            putParcelable(SUPER_STATE_KEY, super.onSaveInstanceState())
            putParcelable(CAR_STATE_KEY, carScreenPosition)
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var newState = state
        if (newState is Bundle) {
            val car: CarScreenPosition? = newState.getParcelable(CAR_STATE_KEY)
            if (car != null) {
                carScreenPosition = car
            }
            newState = newState.getParcelable(SUPER_STATE_KEY)
        }
        super.onRestoreInstanceState(newState)
    }
}