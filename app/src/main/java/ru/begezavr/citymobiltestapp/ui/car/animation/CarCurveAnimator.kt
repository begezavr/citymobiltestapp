package ru.begezavr.citymobiltestapp.ui.car.animation

import android.animation.*
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.PointF
import android.view.animation.AccelerateDecelerateInterpolator
import ru.begezavr.citymobiltestapp.ui.car.view.CarScreenPosition
import kotlin.math.atan2


class CarCurveAnimator(private val listener: (CarScreenPosition) -> Unit) : CarAnimator(listener) {
    companion object {
        private const val MOVE_DURATION = 600L
    }

    private var animator: ValueAnimator? = null
    private var currentPosition: CarScreenPosition = CarScreenPosition.EMPTY
    private var targetPosition: CarScreenPosition = CarScreenPosition.EMPTY
    private var prevPosition: CarScreenPosition = CarScreenPosition.EMPTY
    private val tan = FloatArray(2)
    private var pathMeasure = PathMeasure()
    private var pathListener: ((Path) -> Unit)? = null

    private var point0: PointF = PointF()
    private var point1: PointF = PointF()
    private var point2: PointF = PointF()
    private var point3: PointF = PointF()

    override fun go(from: CarScreenPosition, to: CarScreenPosition) {
        animator?.cancel()
        currentPosition = from
        prevPosition = currentPosition
        targetPosition = to
        startAnimation()
    }

    override fun cancel() {
        animator?.cancel()
    }

    fun getPath(listener: (Path) -> Unit) {
        this.pathListener = listener
    }

    private fun startAnimation() {
        val path = Path()
        point0 = PointF(currentPosition.x, currentPosition.y)
        // todo calculate points to satisfy some conditions, like max curvature and so on
        point1 = PointF(0f, 1000f)
        point2 = PointF(1000f, 0f)
        point3 = PointF(targetPosition.x, targetPosition.y)
        path.moveTo(currentPosition.x, currentPosition.y)
        path.quadTo(point1.x, point1.y, point3.x, point3.y)
        pathListener?.invoke(path)
        pathMeasure = PathMeasure(path, false)
        animator = ObjectAnimator.ofFloat(currentPosition, "x", "y", path).apply {
            duration = MOVE_DURATION
            interpolator = AccelerateDecelerateInterpolator()
            addListener(animationListener)
            addUpdateListener {
                pathMeasure.getPosTan(pathMeasure.length * it.animatedFraction, null, tan)
                val targetAngle= 90f + Math.toDegrees(atan2(tan[1], tan[0]).toDouble()).toFloat()
                currentPosition.angle = closestAngle(currentPosition.angle, targetAngle)
                prevPosition = currentPosition
                listener.invoke(currentPosition)
            }
        }
        animator?.start()
    }

    private val animationListener = object: AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            currentPosition.angle = cleanAngle(currentPosition.angle)
            listener.invoke(currentPosition)
        }
    }
}
