package ru.begezavr.citymobiltestapp.ui.car.view

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import ru.begezavr.citymobiltestapp.util.dp

class SimpleCar(private val fillColor: Int, private val borderColor: Int): Car() {
    private val paint: Paint = Paint()
    private val strokeWidth = 2.dp
    private val rect = RectF()

    override fun draw(canvas: Canvas, screenPosition: CarScreenPosition) {
        canvas.save()
        canvas.rotate(screenPosition.angle, screenPosition.x, screenPosition. y)

        val halfWidth = screenPosition.width / 2
        val halfHeight = screenPosition.height / 2
        rect.set(screenPosition.x - halfWidth,
                screenPosition.y - halfHeight,
                screenPosition.x + halfWidth,
                screenPosition.y + halfHeight)

        // fill
        paint.style = Paint.Style.FILL
        paint.color = fillColor
        canvas.drawRect(rect, paint)

        // border
        paint.style = Paint.Style.STROKE
        paint.color = borderColor
        paint.strokeWidth = strokeWidth
        canvas.drawRect(rect, paint)

        canvas.restore()
    }
}