package ru.begezavr.citymobiltestapp.ui.car.view

import android.graphics.Canvas
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * This class represent drawing box coordinates for car.
 */
@Parcelize
data class CarScreenPosition(var x: Float, var y: Float,
                             val width: Float, val height: Float,
                             var angle: Float): Parcelable {
    companion object {
        val EMPTY = CarScreenPosition(0f, 0f, 0f, 0f, 0f)
    }
}

abstract class Car {
    abstract fun draw(canvas: Canvas, screenPosition: CarScreenPosition)
}
