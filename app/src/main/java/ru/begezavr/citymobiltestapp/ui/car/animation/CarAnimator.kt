package ru.begezavr.citymobiltestapp.ui.car.animation

import ru.begezavr.citymobiltestapp.ui.car.view.CarScreenPosition

/**
 * Class for animate CarScreenPosition. Use method go to start animation.
 */
abstract class CarAnimator(listener: (CarScreenPosition) -> Unit) {
    abstract fun go(from: CarScreenPosition, to: CarScreenPosition)
    abstract fun cancel()
}