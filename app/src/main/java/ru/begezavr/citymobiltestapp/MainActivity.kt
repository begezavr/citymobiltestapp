package ru.begezavr.citymobiltestapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ru.begezavr.citymobiltestapp.ui.car.CarFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CarFragment.newInstance())
                .commitNow()
        }
    }

}
